# simpleModelServer

利用C++ Web Server和Tensorflow Serving构建一个简单的Model Server

整体的架构图如下：

[//]:(![整体架构图](framework.png))

<div align="center">
    <img src="./framework.png", width="800">
</div>